import java.util.Scanner;
import java.io.*;  
import java.util.*;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.nio.file.Files;
import java.nio.file.Paths;

class GetAllFeatures{
	public static void main(String[]args)
	{

		String curDir = System.getProperty("user.dir");
		getAllFiles(curDir);
		generatepagefiles(curDir);
	}
	private static void generatepagefiles(String curDir) {
		try{

			String sysDir = System.getProperty("user.home");
			File featurepath = new File(curDir+"/src/test/resources/htmlfiles");
			File[] filesList = featurepath.listFiles();
			for(File f : filesList){
				String htmlfile = f.getName();
				int pos = htmlfile.lastIndexOf(".");
				String htmlname = htmlfile.substring(0, pos);
				htmlname = htmlname.substring(0, 1).toUpperCase() + htmlname.substring(1);
				htmlname = htmlname.replaceAll("[-]", "_");
				String cmd = sysDir+"\\node_modules\\.bin\\selenium-page-object-generator -t java -n "+htmlname+"Page  -d "+curDir+" -s "+curDir+"\\src\\test\\resources\\htmlfiles\\"+htmlfile;
				System.out.println(cmd);
				Runtime.getRuntime().exec("cmd /c start cmd.exe /C "+cmd); 
				Thread.sleep(1000);	
				try{			
					Files.move(Paths.get(curDir+"\\"+htmlname+"Page.java").toAbsolutePath(),Paths.get(curDir+"\\src\\test\\java\\pages\\"+htmlname+"Page.java").toAbsolutePath());
					String filename=curDir+"\\src\\test\\java\\pages\\"+htmlname+"Page.java"    ;   
					FileReader fis = new FileReader(filename);
					BufferedReader br = new BufferedReader(fis);
					String result = "";
					String line = "";
					while( (line = br.readLine()) != null){
						if(line.contains(curDir)){
							line = line.replace(curDir,htmlname+"Page");
						}
						if(line.contains("private final String pageUrl")) {
							line = "private final String pageUrl = "+'"'+'"'+";";
						}
						String name = "public class "+htmlname+"Page extends DriverSetupUtility{";
						String genname = "public class "+htmlname+"Page {";
						String pagesetup = "public "+htmlname+"Page() {\n this.driver=driver();";
						if(line.contains(genname)){
							line = line.replace(genname,name);
						}
						if(line.contains("public "+htmlname+"Page() {")){
							line = line.replace("public "+htmlname+"Page() {",pagesetup);
						}
						result = result + "\n"+line; 
					}



					result = "package pages;\nimport org.openqa.selenium.support.PageFactory;\nimport utilities.DriverSetupUtility;\n" + result;

					//filename.delete();
					FileOutputStream fos = new FileOutputStream(filename);
					fos.write(result.getBytes());
					fos.flush();
				}

				catch(FileNotFoundException fe){
					System.out.println(fe);
					System.out.println("\n");
					System.out.println(fe+ "\n Something went wrong! please try again");

				}	

			}

		}
		catch(Exception e){
			System.out.println("Something went wrong! please try again");
		}

	}
	private static void getAllFiles(String curDir) {
		try{
			File featurepath = new File(curDir+"/src/test/resources/features");
			File[] filesList = featurepath.listFiles();
			for(File f : filesList){
				String featurefile = f.getName();
				int pos = featurefile.lastIndexOf(".");
				String featurename = featurefile.substring(0, pos);
				featurename = featurename.substring(0, 1).toUpperCase() + featurename.substring(1);
				String stepsfilename = curDir+"/src/test/java/stepdefs/"+featurename+"Steps.java";
				String testsfilename = curDir+"/src/test/java/tests/"+featurename+".java";
				String pagefilename = curDir+"/src/test/java/pages/"+featurename+"Page.java";				
				File stepfile = new File(stepsfilename);
				File testfile = new File(testsfilename);
				stepfile.createNewFile();
				testfile.createNewFile();
				FileWriter stepfileWriter = null;
				FileWriter pagefileWriter = null;
				FileWriter testfileWriter = null;
				BufferedWriter stepbufferedWriter = null;

				BufferedWriter testbufferedWriter = null;
				PrintWriter stepswriter = null;

				PrintWriter testwriter = null;
				File myObj = new File(featurepath+"\\"+featurename+".feature");

				stepfileWriter = new FileWriter(stepsfilename,true);
				//pagefileWriter = new FileWriter(pagefilename,true);
				testfileWriter = new FileWriter(testsfilename,true);
				Scanner myReader = new Scanner(myObj);  
				Scanner stepReader = new Scanner(stepfile);
				//Wrapping FileWriter object in BufferedWriter
				stepbufferedWriter = new BufferedWriter(stepfileWriter);
				//pagebufferedWriter = new BufferedWriter(pagefileWriter);
				testbufferedWriter = new BufferedWriter(testfileWriter);
				//Wrapping BufferedWriter object in PrintWriter
				stepswriter = new PrintWriter(stepbufferedWriter);
				//pagewriter = new PrintWriter(pagebufferedWriter);
				testwriter = new PrintWriter(testbufferedWriter);
				stepswriter.println("package stepdefs;");
				stepswriter.println("import cucumber.api.java.en.*;");
				stepswriter.println("import pages.*;");
				stepswriter.println("import utilities.*;");
				stepswriter.println("import org.openqa.selenium.WebDriver;");
				stepswriter.println("import cucumber.api.java.*;");
				stepswriter.println("import org.testng.annotations.*;");
				stepswriter.println("import org.openqa.selenium.support.PageFactory;");
				stepswriter.println("public class "+featurename+"Steps {");
				testwriter.println("package tests;");
				testwriter.println("import stepdefs.*;");
				testwriter.println("import org.testng.annotations.*;");
				testwriter.println("public class "+featurename+" extends "+featurename+"Steps {");
				stepswriter.println();
				String direc = curDir.replace(File.separator,"\\\\");
				String dec = "DriverSetupUtility setup = new DriverSetupUtility();\nFileUtility filesetup = new FileUtility();\n "+featurename+"Page "+featurename.toLowerCase()+";\n	WebDriver driver;\n	String url;\n String confFile = "+'"'+direc+"\\\\src\\\\test\\\\resources\\\\config.properties"+'"'+";" ;
				String beforestepsetup = "@Before \n public void initializesetup(){\n url = FileUtility.getPropertyValue(confFile,"+'"'+" "+'"'+");\n driver = setup.openBrowser(url);\n "+featurename.toLowerCase()+" = PageFactory.initElements(driver,"+featurename+"Page.class); \n }\n";
				String beforetestsetup = "@BeforeTest \n public void setup(){\n url = FileUtility.getPropertyValue(confFile,"+'"'+" "+'"'+");\n driver = setup.openBrowser(url);\n "+featurename.toLowerCase()+" = PageFactory.initElements(driver,"+featurename+"Page.class); \n }\n @AfterTest \n public void testteardown(){\n setup.closeBrowser();\n}";

				stepswriter.println(dec);
				stepswriter.println();
				stepswriter.println(beforestepsetup);
				stepswriter.println(beforetestsetup);
				String isclassclosed = "false";

				stepswriter.println();
				//pagewriter.println();
				testwriter.println();
				String given = "ll";
				String when = "ll";
				String then = "ll";
				String and = "ll";

				while (myReader.hasNextLine()) {
					String data = myReader.nextLine();
					int point = 0;
					if(data.contains("Scenario")) {
						String[] givend = data.split("Scenario");
						System.out.println(givend[1]);
						String state = givend[1].replace(' ','_');
						state = state.replace(':',' ');
						//Writing text to file
						if(point!=0){
							testwriter.println("}");
						}
						testwriter.println("@Test");
						testwriter.println("public void "+state+"() {");
						testwriter.println();
						point = point+1;
					}
					if(data.contains("Given")) {

						String[] givend = data.split("Given");
						System.out.println(givend[1]);
						String state = givend[1].replace(' ','_');
						//Writing text to file
						if(!(given.contains(givend[1]))){
							stepswriter.println("@Given("+'"'+"^"+givend[1].trim()+"$"+'"'+")");
							stepswriter.println("public void "+state+"() {}");
							stepswriter.println();
							given= givend[1];
						}
						testwriter.println(state+"();");
						testwriter.println();

					}
					if(data.contains("When")) {
						String[] givend = data.split("When");
						System.out.println(givend[1]);
						String state = givend[1].replace(' ','_');
						//Writing text to file
						if(!(when.contains(givend[1]))){
							stepswriter.println("@When("+'"'+"^"+givend[1].trim()+"$"+'"'+")");
							stepswriter.println("public void "+state+"() {}");
							stepswriter.println();
							when= givend[1];
						}
						testwriter.println(state+"();");
						testwriter.println();

					}
					if(data.contains("Then")) {
						String[] givend = data.split("Then");
						System.out.println(givend[1]);
						String state = givend[1].replace(' ','_');
						//Writing text to file
						if(!(then.contains(givend[1]))){
							stepswriter.println("@Then("+'"'+"^"+givend[1].trim()+"$"+'"'+")");
							stepswriter.println("public void "+state+"() {}");
							stepswriter.println();
							then= givend[1];
						}
						testwriter.println(state+"();");
						testwriter.println();

					}
					if(data.contains("And")) {
						String[] givend = data.split("And");
						System.out.println(givend[1]);
						String state = givend[1].replace(' ','_');
						//Writing text to file
						if(!(and.contains(givend[1]))){
							stepswriter.println("@And("+'"'+"^"+givend[1].trim()+"$"+'"'+")");
							stepswriter.println("public void "+state+"() {}");
							stepswriter.println();
							and= givend[1];
						}
						testwriter.println(state+"();");
						testwriter.println();
					}

				}
				testwriter.println("}");
				stepswriter.println("}");
				stepswriter.close();
				stepbufferedWriter.close();
				stepfileWriter.close();
				//pagewriter.println("}");
				//pagewriter.close();
				//pagebufferedWriter.close();
				//pagefileWriter.close();
				testwriter.println("}");
				testwriter.close();
				testbufferedWriter.close();
				testfileWriter.close();

				myReader.close();


			} 


		}
		catch (Exception e)   {  
			e.printStackTrace();    //prints exception if any  
		}

	}
}