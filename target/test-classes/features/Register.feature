@register
Feature: Register
  I want to create account on msnagile account

  @smoke
  Scenario: Verify that user is able to register
    Given I browse to the msnagile register page
    When I enter signup details and hit submit
    Then the user should be registered successfully
