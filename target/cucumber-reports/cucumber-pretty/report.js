$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Register.feature");
formatter.feature({
  "line": 2,
  "name": "Register",
  "description": "I want to create account on msnagile account",
  "id": "register",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@register"
    }
  ]
});
formatter.scenario({
  "line": 6,
  "name": "Verify that user is able to register",
  "description": "",
  "id": "register;verify-that-user-is-able-to-register",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "I browse to the msnagile register page",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I enter signup details and hit submit",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "the user should be registered successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "RegisterSteps._I_browse_to_the_msnagile_register_page()"
});
formatter.result({
  "duration": 16949018700,
  "status": "passed"
});
formatter.match({
  "location": "RegisterSteps._I_enter_signup_details_and_hit_submit()"
});
formatter.result({
  "duration": 31299,
  "status": "passed"
});
formatter.match({
  "location": "RegisterSteps._the_user_should_be_registered_successfully()"
});
formatter.result({
  "duration": 35200,
  "status": "passed"
});
formatter.after({
  "duration": 881121601,
  "status": "passed"
});
formatter.uri("login.feature");
formatter.feature({
  "line": 2,
  "name": "Login",
  "description": "I want to access msnagile application",
  "id": "login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@login"
    }
  ]
});
formatter.scenario({
  "line": 6,
  "name": "Verify that user is able to login with valid credentials",
  "description": "",
  "id": "login;verify-that-user-is-able-to-login-with-valid-credentials",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "I browse to the msnagile login page",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I enter email and password and hit login",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "the user should be logged in successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps._I_browse_to_the_msnagile_login_page()"
});
formatter.result({
  "duration": 15980928199,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps._I_enter_email_and_password_and_hit_login()"
});
formatter.result({
  "duration": 28800,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps._the_user_should_be_logged_in_successfully()"
});
formatter.result({
  "duration": 23501,
  "status": "passed"
});
formatter.after({
  "duration": 990104999,
  "status": "passed"
});
});