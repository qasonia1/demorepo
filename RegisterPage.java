import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegisterPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(id = "loginWithGoogle")
    @CacheLookup
    private WebElement continueWithGoogle;

    @FindBy(id = "createAccount")
    @CacheLookup
    private WebElement createbtntext;

    private final String pageLoadedText = "Please enter a valid email address";

    private final String pageUrl = "/C:/Users/Devstringx/Documents/JavaWorkspace/CAF/src/test/resources/htmlfiles/register.html";

    @FindBy(id = "domainName")
    @CacheLookup
    private WebElement pleaseEnterAValidDomain;

    @FindBy(id = "fullname")
    @CacheLookup
    private WebElement pleaseEnterAValidEmailAddress1;

    @FindBy(id = "email")
    @CacheLookup
    private WebElement pleaseEnterAValidEmailAddress2;

    @FindBy(id = "password")
    @CacheLookup
    private WebElement pleaseEnterAValidEmailAddress3;

    public RegisterPage() {
    }

    public RegisterPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public RegisterPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public RegisterPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Continue With Google Button.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage clickContinueWithGoogleButton() {
        continueWithGoogle.click();
        return this;
    }

    /**
     * Click on Createbtntext Button.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage clickCreatebtntextButton() {
        createbtntext.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage fill() {
        setPleaseEnterAValidEmailAddress1TextField();
        setPleaseEnterAValidEmailAddress2TextField();
        setPleaseEnterAValidEmailAddress3PasswordField();
        setPleaseEnterAValidDomainTextField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the C:\Users\Devstringx\Documents\JavaWorkspace\CAF class instance.
     */
    public C:\Users\Devstringx\Documents\JavaWorkspace\CAF fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Please Enter A Valid Domain Text field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidDomainTextField() {
        return setPleaseEnterAValidDomainTextField(data.get("PLEASE_ENTER_A_VALID_DOMAIN"));
    }

    /**
     * Set value to Please Enter A Valid Domain Text field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidDomainTextField(String pleaseEnterAValidDomainValue) {
        pleaseEnterAValidDomain.sendKeys(pleaseEnterAValidDomainValue);
        return this;
    }

    /**
     * Set default value to Please Enter A Valid Email Address Text field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidEmailAddress1TextField() {
        return setPleaseEnterAValidEmailAddress1TextField(data.get("PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_1"));
    }

    /**
     * Set value to Please Enter A Valid Email Address Text field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidEmailAddress1TextField(String pleaseEnterAValidEmailAddress1Value) {
        pleaseEnterAValidEmailAddress1.sendKeys(pleaseEnterAValidEmailAddress1Value);
        return this;
    }

    /**
     * Set default value to Please Enter A Valid Email Address Text field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidEmailAddress2TextField() {
        return setPleaseEnterAValidEmailAddress2TextField(data.get("PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_2"));
    }

    /**
     * Set value to Please Enter A Valid Email Address Text field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidEmailAddress2TextField(String pleaseEnterAValidEmailAddress2Value) {
        pleaseEnterAValidEmailAddress2.sendKeys(pleaseEnterAValidEmailAddress2Value);
        return this;
    }

    /**
     * Set default value to Please Enter A Valid Email Address Password field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidEmailAddress3PasswordField() {
        return setPleaseEnterAValidEmailAddress3PasswordField(data.get("PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_3"));
    }

    /**
     * Set value to Please Enter A Valid Email Address Password field.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage setPleaseEnterAValidEmailAddress3PasswordField(String pleaseEnterAValidEmailAddress3Value) {
        pleaseEnterAValidEmailAddress3.sendKeys(pleaseEnterAValidEmailAddress3Value);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the C:\Users\Devstringx\Documents\JavaWorkspace\CAF class instance.
     */
    public C:\Users\Devstringx\Documents\JavaWorkspace\CAF submit() {
        clickCreatebtntextButton();
        C:\Users\Devstringx\Documents\JavaWorkspace\CAF target = new C:\Users\Devstringx\Documents\JavaWorkspace\CAF(driver, data, timeout);
        PageFactory.initElements(driver, target);
        return target;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the RegisterPage class instance.
     */
    public RegisterPage verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
