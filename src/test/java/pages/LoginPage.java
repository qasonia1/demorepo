package pages;
import org.openqa.selenium.support.PageFactory;
import utilities.DriverSetupUtility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends DriverSetupUtility{
	private Map<String, String> data;
	private WebDriver driver;
	private int timeout = 15;

	@FindBy(id = "continue-button")
	@CacheLookup
	private WebElement continueWithGoogle;

	@FindBy(id = "forgot-password")
	@CacheLookup
	private WebElement forgotPassword;

	@FindBy(id = "loginBtbn")
	@CacheLookup
	private WebElement login;

	private final String pageLoadedText = "Log in to your account";

	private final String pageUrl = "";

	@FindBy(id = "email")
	@CacheLookup
	private WebElement pleaseEnterAValidEmailAddress1;

	@FindBy(id = "password")
	@CacheLookup
	private WebElement pleaseEnterAValidEmailAddress2;

	public LoginPage() {
		this.driver=driver();
		data = new HashMap<String, String>();
		data.put("EMAIL","shivangi.awasthi@devstringx.com");
		data.put("PASSWORD","123123");
	}

	public LoginPage(WebDriver driver) {
		this();
		this.driver = driver;
	}

	public LoginPage(WebDriver driver, Map<String, String> data) {
		this(driver);
		this.data = data;
	}

	public LoginPage(WebDriver driver, Map<String, String> data, int timeout) {
		this(driver, data);
		this.timeout = timeout;
	}

	/**
	 * Click on Continue With Google Button.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage clickContinueWithGoogleButton() {
		continueWithGoogle.click();
		return this;
	}

	/**
	 * Click on Forgot Password Link.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage clickForgotPasswordLink() {
		forgotPassword.click();
		return this;
	}

	/**
	 * Click on Login Button.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage clickLoginButton() {
		login.click();
		return this;
	}

	/**
	 * Fill every fields in the page.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage fill() {
		setPleaseEnterAValidEmailAddress1PasswordField();
		setPleaseEnterAValidEmailAddress2PasswordField();
		return this;
	}

	/**
	 * Fill every fields in the page and submit it to target page.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage fillAndSubmit() {
		fill();
		return submit();
	}

	/**
	 * Set default value to Please Enter A Valid Email Address Password field.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage setPleaseEnterAValidEmailAddress1PasswordField() {
		return setPleaseEnterAValidEmailAddress1PasswordField(data.get("EMAIL"));
	}

	/**
	 * Set value to Please Enter A Valid Email Address Password field.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage setPleaseEnterAValidEmailAddress1PasswordField(String pleaseEnterAValidEmailAddress1Value) {
		pleaseEnterAValidEmailAddress1.sendKeys(pleaseEnterAValidEmailAddress1Value);
		return this;
	}

	/**
	 * Set default value to Please Enter A Valid Email Address Password field.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage setPleaseEnterAValidEmailAddress2PasswordField() {
		return setPleaseEnterAValidEmailAddress2PasswordField(data.get("PASSWORD"));
	}

	/**
	 * Set value to Please Enter A Valid Email Address Password field.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage setPleaseEnterAValidEmailAddress2PasswordField(String pleaseEnterAValidEmailAddress2Value) {
		pleaseEnterAValidEmailAddress2.sendKeys(pleaseEnterAValidEmailAddress2Value);
		return this;
	}

	/**
	 * Submit the form to target page.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage submit() {
		clickLoginButton();
		LoginPage target = new LoginPage(driver, data, timeout);
		PageFactory.initElements(driver, target);
		return target;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage verifyPageLoaded() {
		(new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getPageSource().contains(pageLoadedText);
			}
		});
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the LoginPage class instance.
	 */
	public LoginPage verifyPageUrl() {
		(new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getCurrentUrl().contains(pageUrl);
			}
		});
		return this;
	}
}