package pages;
import org.openqa.selenium.support.PageFactory;
import utilities.DriverSetupUtility;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Active_sprintsPage extends DriverSetupUtility{
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(id = "iconBtn")
    @CacheLookup
    private WebElement closeSprint;

    private final String pageLoadedText = "";

    private final String pageUrl = "/C:/Users/Devstringx/Documents/JavaWorkspace/CAF/src/test/resources/htmlfiles/active-sprints.html";

    public Active_sprintsPage() {
 this.driver=driver();
    }

    public Active_sprintsPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public Active_sprintsPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public Active_sprintsPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Close Sprint Button.
     *
     * @return the Active_sprintsPage class instance.
     */
    public Active_sprintsPage clickCloseSprintButton() {
        closeSprint.click();
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the Active_sprintsPage class instance.
     */
    public Active_sprintsPage verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the Active_sprintsPage class instance.
     */
    public Active_sprintsPage verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}