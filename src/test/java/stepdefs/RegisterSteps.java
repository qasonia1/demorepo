package stepdefs;
import cucumber.api.java.en.*;
import pages.*;
import utilities.*;
import org.openqa.selenium.WebDriver;
import cucumber.api.java.*;
import org.testng.annotations.*;
import org.openqa.selenium.support.PageFactory;
public class RegisterSteps {

DriverSetupUtility setup = new DriverSetupUtility();
FileUtility filesetup = new FileUtility();
 RegisterPage register;
	WebDriver driver;
	String url;
 String confFile = "C:\\Users\\Devstringx\\Documents\\JavaWorkspace\\CAF\\src\\test\\resources\\config.properties";

@Before 
 public void initializesetup(){
 url = FileUtility.getPropertyValue(confFile," ");
 driver = setup.openBrowser(url);
 register = PageFactory.initElements(driver,RegisterPage.class); 
 }

@BeforeTest 
 public void setup(){
 url = FileUtility.getPropertyValue(confFile," ");
 driver = setup.openBrowser(url);
 register = PageFactory.initElements(driver,RegisterPage.class); 
 }
 @AfterTest 
 public void testteardown(){
 setup.closeBrowser();
}

@Given("^I browse to the msnagile register page$")
public void _I_browse_to_the_msnagile_register_page() {}

@When("^I enter signup details and hit submit$")
public void _I_enter_signup_details_and_hit_submit() {}

@Then("^the user should be registered successfully$")
public void _the_user_should_be_registered_successfully() {}

}
