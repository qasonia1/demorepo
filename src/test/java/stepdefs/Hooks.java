package stepdefs;

import org.openqa.selenium.WebDriver;
import com.cucumber.listener.Reporter;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import utilities.DriverSetupUtility;
import utilities.GeneralUtility;


public class Hooks {

	GeneralUtility general = new GeneralUtility();
	WebDriver driver;
	DriverSetupUtility driversetup = new DriverSetupUtility();


	@After
	public void embedScreenshot(Scenario scenario) {
		driver = driversetup.driver();
		if (scenario.isFailed()) {
			try {
				String path = general.getScreenshot(driver, scenario.getClass().getName());
				Reporter.addScreenCaptureFromPath(path);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		driversetup.closeBrowser();
	}
}
