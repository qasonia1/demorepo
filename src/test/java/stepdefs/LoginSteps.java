package stepdefs;
import cucumber.api.java.en.*;
import pages.*;
import utilities.*;
import org.openqa.selenium.WebDriver;
import cucumber.api.java.*;
import org.testng.annotations.*;
import org.openqa.selenium.support.PageFactory;
public class LoginSteps {

DriverSetupUtility setup = new DriverSetupUtility();
FileUtility filesetup = new FileUtility();
 LoginPage login;
	WebDriver driver;
	String url;
 String confFile = "C:\\Users\\Devstringx\\Documents\\JavaWorkspace\\CAF\\src\\test\\resources\\config.properties";

@Before 
 public void initializesetup(){
 url = FileUtility.getPropertyValue(confFile,"loginUrl");
 driver = setup.openBrowser(url);
 login = PageFactory.initElements(driver,LoginPage.class); 
 }

@BeforeTest 
 public void setup(){
 url = FileUtility.getPropertyValue(confFile,"loginUrl");
 driver = setup.openBrowser(url);
 login = PageFactory.initElements(driver,LoginPage.class); 
 }
 @AfterTest 
 public void testteardown(){
 setup.closeBrowser();
}

@Given("^I browse to the msnagile login page$")
public void _I_browse_to_the_msnagile_login_page() {
	login.verifyPageLoaded();
}

@When("^I enter email and password and hit login$")
public void _I_enter_email_and_password_and_hit_login() {
	login.fillAndSubmit();
}

@Then("^the user should be logged in successfully$")
public void _the_user_should_be_logged_in_successfully() {}

}
