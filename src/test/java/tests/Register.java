package tests;
import stepdefs.*;
import org.testng.annotations.*;
public class Register extends RegisterSteps {

@Test
public void  _Verify_that_user_is_able_to_register() {

_I_browse_to_the_msnagile_register_page();

_I_enter_signup_details_and_hit_submit();

_the_user_should_be_registered_successfully();

}
}
