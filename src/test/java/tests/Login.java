package tests;
import stepdefs.*;
import org.testng.annotations.*;
public class Login extends LoginSteps {

@Test
public void  _Verify_that_user_is_able_to_login_with_valid_credentials() {

_I_browse_to_the_msnagile_login_page();

_I_enter_email_and_password_and_hit_login();

_the_user_should_be_logged_in_successfully();

}
}
