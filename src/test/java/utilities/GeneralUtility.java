package utilities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class GeneralUtility {

	//Creating a method getScreenshot and passing two parameters
	//driver and screenshotName
	public String getScreenshot(WebDriver driver, String screenshotName) {
		String destination = null;
		try {
			//below line is just to append the date format with the screenshot name to avoid duplicate names
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			//after execution, you could see a folder "FailedTestsScreenshots" under src folder
			destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+screenshotName+dateName+".png";
			File finalDestination = new File(destination);
			FileUtils.copyFile(source, finalDestination);
			//Returns the captured file path

		} 
		catch(Exception e) {
			System.out.print(e.getMessage());

		}
		return destination;

	}
}
