package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class FileUtility {


	/**
	 * This method use to create to fetch value from property file of given key.
	 * @param fileName- name with path of property file
	 * @param key - key to fetch value
	 * @return value of key provided
	 */
	public static String getPropertyValue(String fileName, String key) {
		File file = new File(fileName);
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		//load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty(key);
	}

}

