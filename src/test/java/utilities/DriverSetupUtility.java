package utilities;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class DriverSetupUtility extends Interface{

	//private WebDriver driver;
	private String browsername;
	private String isMaximiseWindow;
	private String servername;
	private String username;
	private String accesskey;
	private String gridURL;
	public String appURL;
	GeneralUtility general = new GeneralUtility();


	private String confFile ;

	public DriverSetupUtility() {
		String curDir = System.getProperty("user.dir");
		confFile = curDir+"\\src\\test\\resources\\config.properties";
		browsername = FileUtility.getPropertyValue(confFile,"browser");
		isMaximiseWindow = FileUtility.getPropertyValue(confFile,"ismaximise");
		servername = FileUtility.getPropertyValue(confFile,"cloudservername");
		username = FileUtility.getPropertyValue(confFile,"serverusername");
		accesskey = FileUtility.getPropertyValue(confFile,"serveraccesskey");
		gridURL = FileUtility.getPropertyValue(confFile,"gridURL");
		appURL = FileUtility.getPropertyValue(confFile,"appURL");
	}
	
	private final void setdriver(String url) {

		if(browsername.equals("ch")) {
			// Instantiate a ChromeDriver class.       
			driver=new ChromeDriver();  
			driver.get(url);
			//Maximize the browser
			if(isMaximiseWindow != "False") {  
				driver.manage().window().maximize();  
			}

		}
		else if(browsername.equals("ff")) {
			// Instantiate a Firefox class.       
			driver=new FirefoxDriver();  
			driver.get(url);
			//Maximize the browser
			if(isMaximiseWindow != "False") {  
				driver.manage().window().maximize();  
			}
		}
		else if(browsername.equals("edge")) {
			// Instantiate a Edge class.       
			driver=new EdgeDriver(); 
			driver.get(url);
			//Maximize the browser
			if(isMaximiseWindow != "False") {  
				driver.manage().window().maximize();  
			}
		}
		else {
			Assert.fail("browsername "+ browsername+" is not matched.It must be ch for chrome or ff for firefox or edge for edge browser.");
		}

	}
	
	private final void setdriver() {

		if(browsername.equals("ch")) {
			// Instantiate a ChromeDriver class.       
			driver=new ChromeDriver();  
			//Maximize the browser
			if(isMaximiseWindow != "False") {  
				driver.manage().window().maximize();  
			}

		}
		else if(browsername.equals("ff")) {
			// Instantiate a Firefox class.       
			driver=new FirefoxDriver();  
			//Maximize the browser
			if(isMaximiseWindow != "False") {  
				driver.manage().window().maximize();  
			}
		}
		else if(browsername.equals("edge")) {
			// Instantiate a Edge class.       
			driver=new EdgeDriver(); 
			//Maximize the browser
			if(isMaximiseWindow != "False") {  
				driver.manage().window().maximize();  
			}
		}
		else {
			Assert.fail("browsername "+ browsername+" is not matched.It must be ch for chrome or ff for firefox or edge for edge browser.");
		}

	}

	/**
	 * This method use to create to get current driver
	 * @return driver
	 */
	public final WebDriver driver() {
		return driver;
	}

	/**
	 * This method use to create to open browser on windows.
	 * @return driver
	 */
	public final WebDriver openBrowser(String url) {
		setdriver(url);
		return driver;
	}
	
	/**
	 * This method use to create to open browser on windows.
	 * @return driver
	 */
	public final WebDriver openBrowser() {
		setdriver();
		return driver;
	}
	
	public final void closeBrowser() {
		driver.quit();
	}


	private DesiredCapabilities serverSetup() {
		DesiredCapabilities caps = new DesiredCapabilities();

		if(servername.equals("browserstack")) {
			caps.setCapability("os", "Windows");
			caps.setCapability("os_version", "10");
			caps.setCapability("browser", browsername);
			caps.setCapability("browser_version", "latest-beta");
			caps.setCapability("browserstack.local", "false");
			caps.setCapability("browserstack.selenium_version", "3.14.0");
		}
		else if(servername.equals("crossbrowsertesting")) {
			caps.setCapability("name", "CBT Java");
			caps.setCapability("browserName", browsername);
			caps.setCapability("browserVersion", "84");
			caps.setCapability("platformName", "Windows 10");
		}
		else if(servername.equals("lambdatest")) {
			caps.setCapability("browserName", browsername);
			caps.setCapability("version", "84");
			caps.setCapability("platform", "win10"); // If  cap isn't specified, it will just get any available one
			caps.setCapability("build", "LambdaTestCAFBuild");
			caps.setCapability("name", "LambdaTestCAFTest");
			caps.setCapability("network", true); // To enable network logs
			caps.setCapability("visual", true); // To enable step by step screenshot
			caps.setCapability("video", true); // To enable video recording
			caps.setCapability("console", true); // To capture console logs

			caps.setCapability("selenium_version","4.0.0-alpha-2");
			caps.setCapability("timezone","UTC+05:30");
			caps.setCapability("geoLocation","IN");
			caps.setCapability("chrome.driver","78.0");
		}
		else {
			Assert.fail("servername "+ servername+" is not matched.It must be browserstack or crossbrowsertesting or lambdatest.");
		}
		return caps;	
	}


	/**
	 * This method use to create to open browser on cloud server, like browserstack,lambdatest or crossbrowsertesting
	 * @return driver
	 */
	public WebDriver openBrowserOnServer() {
		DesiredCapabilities caps = serverSetup();
		try {
			driver = new RemoteWebDriver(new URL("https://" + username + ":" + accesskey + "@hub."+ gridURL +":80/wd/hub"), caps);
		} catch (MalformedURLException e) {
			System.out.println("Invalid grid URL");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return driver;
	}
	
}





