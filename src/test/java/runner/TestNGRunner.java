package runner;

import com.relevantcodes.extentreports.LogStatus;
import utilities.*;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGListener;
import org.testng.ITestResult;	

import org.testng.ITestContext ;		
import org.testng.ITestListener ;		
import org.testng.ITestResult ;		

public class TestNGRunner implements ITestListener						
{		

	
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		ExtentTestManager.startTest(context.getName());
	}


	public void onFinish(ITestContext iTestContext) {
		//Do tier down operations for extentreports reporting!
		ExtentTestManager.endTest();
		ExtentTestManager.extent.flush();
	}


	public void onTestStart(ITestResult iTestResult) {
		}


	public void onTestSuccess(ITestResult iTestResult) {
		//ExtentReports log operation for passed tests.
		ExtentTestManager.getTest().log(LogStatus.PASS,iTestResult.getName()+"is passed");
	}


	public void onTestFailure(ITestResult iTestResult) {
		System.out.println("I am in onTestFailure method " + iTestResult.getMethod().getConstructorOrMethod().getName() + " failed");

		//Get driver from BaseTest and assign to local webDriver variable.
		Object testClass = iTestResult.getInstance();
		WebDriver webDriver = DriverSetupUtility.driver;

		//Take base64Screenshot screenshot.
		String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) webDriver).
				getScreenshotAs(OutputType.BASE64);

		//ExtentReports log and screenshot operations for failed tests.
		ExtentTestManager.getTest().log(LogStatus.FAIL,iTestResult.getName());
		ExtentTestManager.getTest().log(LogStatus.FAIL,iTestResult.getThrowable().toString(),ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
	}


	public void onTestSkipped(ITestResult iTestResult) {
		//ExtentReports log operation for skipped tests.
		ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
	}


	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}


	





}