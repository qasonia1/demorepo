@login
Feature: Login
  I want to access msnagile application

  @smoke
  Scenario: Verify that user is able to login with valid credentials
    Given I browse to the msnagile login page
    When I enter email and password and hit login
    Then the user should be logged in successfully
