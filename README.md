PreRequisite: 
   Java 8
   Maven
   Npm to install Selenium Page Object Generator
   
How to use:
   1. Add your applications html files under Html Folder of resources.
   2. Add featurefiles and add testcases
   3. Run batchfile - it will auto generate tests,stepdef and pageclasses(included pagelements and common methods).
   4. Modify Page,Stepdef class accordingly
   
How To Run Tests:
   1. To Generate Cucumber Report: Using cucumber-run.xml
   2. To Generate testng Report : Using testng.xml